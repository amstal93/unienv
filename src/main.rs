extern crate clap;
#[macro_use]
extern crate log;
extern crate simple_error;
extern crate simple_logger;
extern crate yaml_rust;

use yaml_rust::YamlLoader;
use std::error::Error;
use clap::{App, Arg};
use log::Level;

pub mod config;

use config::Config;

// type for generic errors
pub type BoxResult<T> = Result<T,Box<dyn Error>>;

fn main() {
    let matches = App::new(option_env!("CARGO_PKG_NAME").unwrap_or("dockerenv"))
        .version(option_env!("CARGO_PKG_VERSION").unwrap_or("unknwon"))
        .author(option_env!("CARGO_PKG_AUTHORS").unwrap_or("unknwon"))
        .about(option_env!("CARGO_PKG_DESCRIPTION").unwrap_or("unknwon"))
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .arg(Arg::with_name("config")
        .help("Sets the config file to use")
        .required(false)
        .index(1))
        .get_matches();

    let file = matches.value_of("config").unwrap_or(".unienv.yaml");

    let verbose = matches.occurrences_of("v");
    let level = match verbose {
        0 => Level::Error,
        1 => Level::Info, // And Warn
        2 | _ => Level::Debug // And Trace
    };
    simple_logger::init_with_level(level).unwrap();

    info!("Using config file: {}", file);
    
    // Open and read file
    match Config::read(file) {
        Ok(config) => { 
            info!("File {} found and opened", file);
            
            let docs = YamlLoader::load_from_str(&config.content).unwrap();
            let doc = &docs[0];
            info!("{:?}", docs);
        },
        Err(_) => {
            error!("File {} was not found", file);
            std::process::exit(1);
        }
    }
}
