use std::fs;
use super::BoxResult;

pub struct Config {
    pub content: String,
}

impl Config {

    /**
     * Read config file
     */
    pub fn read(filename: &str) -> BoxResult<Config> {
        let content = fs::read_to_string(filename)?.parse()?;
        Ok(Config{ content: content })
    }
}

