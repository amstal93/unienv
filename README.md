# Unienv

`unienv` is a tool to create isolated development environments using `containers`.

`Docker` is great to run various projects without installing all their 
dependencies in your global envionment.
With `unienv` you can also retrieve all dev tools in your shell 
without installing them globally.
For a `typescript` project, it can be `yarn`, `jest`, `tslint`...
For a `PHP` project, it can be `composer`, `phpunit` ...

`unienv` will help you switch easily from a project to an other one.

This tool is widely inspired by [virtualenv](https://github.com/pypa/virtualenv) :heart:


## Usage

```bash
unienv
```

It will create a directory `.unienv.d` to place the new virtual environment.

### Activate scripts

Previous commands will also *activate* shell but on already created virtual
environment, you can run :

```bash
source .unienv.d/bin/activate
```

### Deactivate script

To undo these change :

```bash
deactivate
```

Or simply close your shell.

## Configuration

`unienv` will read a file named `.unienv.yaml` in project root dir.

> If you want to use a different file, you can run :
> 
> ```bash
> unienv myfile.yaml
> ```

This file contains all available commands for the virtual environment.

```yaml
docker:
    yarn:
        image: node:12-alpine
    jest:
        build: Dockerfile-jest
    python:
        image: python:2-alpine
        command: python2
    adminer:
        image: adminer
    apache:
        image: httpd:alpine
        volume: /usr/local/apache2/htdocs/
```

This configuration will provide 5 new commands :

* `yarn`
* `jest`
* `python` (alias for `python2`)
* `adminer`
* `apache`

All commands are run with global configuration :

* `--network host` : all `docker` exposed ports are available on host
* `-v $PWD:/~` : all files in current directory are mounted in workdir
* `-u $(id -u):$(id -g)` : command is run as current user

## Other commands

```
unienv -l # List all available commands and services
```

## Installation

Simply copy `unienv` executable in a global `PATH` directory (ie : /usr/local/bin)

## Alternatives

[envirius](https://github.com/ekalinin/envirius) : Universal Virtual Environments Manager
